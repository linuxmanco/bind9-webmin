## Run Container
    sudo docker run -dit -m 100mb --cpus 1 --name bind9-webmin -h bind9-webmin -p 10000:10000 -p 53:53 linuxmanco/bind9-webmin

From there, navigate to `https://<server-ip>:10000` and login to Webmin with:

Username: root
Password: root

You can now manage your domain(s) and manage your DNS server.